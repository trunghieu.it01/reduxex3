import { PLAYER_CHOOSE, PLAY_GAME, TAI, XIU } from "../constant/constant";

let initailState = {
  diceArr: [
    {
      img: "../diceImg/1.png",
      value: 1,
    },
    {
      img: "../diceImg/1.png",
      value: 1,
    },
    {
      img: "../diceImg/1.png",
      value: 1,
    },
  ],
  result: null,
  playerChoose: null,
  totalPlayCount: 0,
  totalWin: 0,
};

export const DiceReducer = (state = initailState, { type, payload }) => {
  switch (type) {
    case PLAY_GAME: {
      let dicePoints = 0;
      let diceResult = "";
      let result = "";
      state.totalPlayCount += 1;
      let randomArrImg = state.diceArr.map((item) => {
        let randomInt = Math.floor(Math.random() * 6) + 1;
        dicePoints += randomInt;
        return {
          img: `../diceImg/${randomInt}.png`,
          value: randomInt,
        };
      });
      if (dicePoints > 11) {
        diceResult = TAI;
      } else {
        diceResult = XIU;
      }
      if (diceResult == state.playerChoose) {
        result = "YOU WIN";
        state.totalWin += 1;
      } else {
        result = "YOU LOSE";
      }

      return {
        ...state,
        diceArr: randomArrImg,
        result: result,
      };
    }
    case PLAYER_CHOOSE: {
      let playerChoose = "";
      if (payload.betType == TAI) {
        playerChoose = TAI;
      } else {
        playerChoose = XIU;
      }
      return { ...state, playerChoose: playerChoose };
    }
    default:
      return state;
  }
};
