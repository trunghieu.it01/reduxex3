import React, { Component } from "react";
import { connect } from "react-redux";
import { PLAYER_CHOOSE, TAI, XIU } from "./redux/constant/constant";

let styleButton = {
  width: 150,
  height: 150,
  fontSize: 40,
};

class Dice extends Component {
  renderDiceArr = () => {
    return this.props.diceArr.map((item) => {
      return <img src={item.img} style={{ width: 100, margin: 10 }} alt="" />;
    });
  };
  render() {
    return (
      <div className="d-flex justify-content-between container pt-5">
        <button
          onClick={() => {
            this.props.handlePlaceBet(TAI);
          }}
          className="btn btn-danger"
          style={styleButton}
        >
          Tài
        </button>
        <div>{this.renderDiceArr()}</div>
        <button
          onClick={() => {
            this.props.handlePlaceBet(XIU);
          }}
          className="btn btn-dark"
          style={styleButton}
        >
          Xỉu
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    diceArr: state.diceReducer.diceArr,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handlePlaceBet: (choosen) => {
      dispatch({
        type: PLAYER_CHOOSE,
        payload: {
          betType: choosen,
        },
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dice);
