import React, { Component } from "react";
import bg_game from "./assets/bgGame.png";
import Dice from "./Dice";
import Result from "./Result";
import "./game.css";

export default class Dice_Layout extends Component {
  render() {
    return (
      <div
        style={{
          backgroundImage: `url(${bg_game})`,
          width: "100vw",
          height: "100vh",
        }}
        className="bg_game"
      >
        <Dice />
        <Result />
      </div>
    );
  }
}
